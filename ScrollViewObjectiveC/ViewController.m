//
//  ViewController.m
//  ScrollViewObjectiveC
//
//  Created by Dane Wikstrom on 8/30/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIScrollView* scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];//Initalizing scrollview memory and allocating, and setting frame
    scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3);
    [scrollview setPagingEnabled:YES];
    [self.view addSubview:scrollview];
    
    UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];//0,0
    view1.backgroundColor = [UIColor redColor];
    [scrollview addSubview:view1];
    
    UILabel* view1Label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view1Label setTextColor:[UIColor whiteColor]];
    [view1Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view1Label.text = @"1";
    [scrollview addSubview:view1Label];
    
    
    UIView* view2 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];//0,1
    view2.backgroundColor = [UIColor blueColor];
    [scrollview addSubview:view2];
    
    UILabel* view2Label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view2Label setTextColor:[UIColor whiteColor]];
    [view2Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view2Label.text = @"2";
    [scrollview addSubview:view2Label];
    
    
    UIView* view3 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, 0, self.view.frame.size.width, self.view.frame.size.height)];//0,2
    view3.backgroundColor = [UIColor purpleColor];
    [scrollview addSubview:view3];
    
    UILabel* view3Label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width*2, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view3Label setTextColor:[UIColor whiteColor]];
    [view3Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view3Label.text = @"3";
    [scrollview addSubview:view3Label];
    
    
    UIView* view4 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];//1,0
    view4.backgroundColor = [UIColor blackColor];
    [scrollview addSubview:view4];
    
    UILabel* view4Label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    [view4Label setTextColor:[UIColor whiteColor]];
    [view4Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view4Label.text = @"4";
    [scrollview addSubview:view4Label];
    
    
    UIView* view5 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];//1,1
    view5.backgroundColor = [UIColor magentaColor];
    [scrollview addSubview:view5];
    
    UILabel* view5Label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    [view5Label setTextColor:[UIColor whiteColor]];
    [view5Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view5Label.text = @"5";
    [scrollview addSubview:view5Label];
    

    UIView* view6 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];//1,2
    view6.backgroundColor = [UIColor orangeColor];
    [scrollview addSubview:view6];
    
    UILabel* view6Label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    [view6Label setTextColor:[UIColor whiteColor]];
    [view6Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view6Label.text = @"6";
    [scrollview addSubview:view6Label];
    

    UIView* view7 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height*2, self.view.frame.size.height, self.view.frame.size.height)];//2,0
    view7.backgroundColor = [UIColor cyanColor];
    [scrollview addSubview:view7];
    
    UILabel* view7Label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height*2, self.view.frame.size.height, self.view.frame.size.height)];
    [view7Label setTextColor:[UIColor whiteColor]];
    [view7Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view7Label.text = @"7";
    [scrollview addSubview:view7Label];
    
    
    UIView* view8 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height*2, self.view.frame.size.height, self.view.frame.size.height)];//2,1
    view8.backgroundColor = [UIColor brownColor];
    [scrollview addSubview:view8];
    
    UILabel* view8Label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height*2, self.view.frame.size.height, self.view.frame.size.height)];
    [view8Label setTextColor:[UIColor whiteColor]];
    [view8Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view8Label.text = @"8";
    [scrollview addSubview:view8Label];
    
    
    UIView* view9 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];//2,2
    view9.backgroundColor = [UIColor grayColor];
    [scrollview addSubview:view9];
    
    UILabel* view9Label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    [view9Label setTextColor:[UIColor whiteColor]];
    [view9Label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 50.0f]];
    view9Label.text = @"9";
    [scrollview addSubview:view9Label];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
